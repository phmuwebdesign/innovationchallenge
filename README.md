# Innovation Challenge Website

Static template for the Innovation Challenge 2019.

## Setup your Dev Environment

1. To develop locally you need Pingy. Install Pingy with `npm install -g @pingy/cli`.
2. Install dependencies `yarn``
3. Start local dev server `pingy dev`
4. Export your changes `pingy dev``

## Deployment

### Deployment to test stage

To deploy your changes to the test server you've only to push the master branch to the remote repo on bitbucket. It will trigger an automatic deployment via DeployHQ.

### Deployment to live stage

To deploy your changes to the live server you've only to push the live branch to the remote repo on bitbucket. It will trigger an automatic deployment via DeployHQ.

